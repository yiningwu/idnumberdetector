# USAGE
# python3 run.py --image path to input image 123456789123456789

# import the necessary packages
from detector.imageprocess import image_processing
import argparse
import cv2
import re
import difflib


ap = argparse.ArgumentParser()
ap.add_argument("echo", help="string of written ID number")
ap.add_argument("-i", "--image", required=True, help="path to input image")
args = vars(ap.parse_args())
pending = None
img = cv2.imread(args["image"])
originalID = args["echo"]
originalID = originalID.upper()
if len(originalID) == 18:
	final_result, ratio = image_processing(img, originalID)
	if originalID == final_result:
		print ("Pass")
	elif (final_result != None) and (len(final_result) >= 15) and (len(final_result) < 18) and (ratio >= 0.9):
		print ("Pass")
	elif final_result != None:
		pending = re.sub('[^X0-9]','', final_result)		
		s = difflib.SequenceMatcher(None, pending, originalID)
		similarity_ratio = s.ratio()
		if (similarity_ratio >= 0.9) and (len(pending)!=18):
			print ("Pass")	
		elif (len(pending) == 18) and (similarity_ratio == 1):
			print ("Pass")
		elif (similarity_ratio < 0.9) and (len(pending) > 18):
			count = 0
			for m in range(len(final_result)):
				for n in range(len(originalID)):
					if (final_result[m] == originalID[n]) :
						count = count + 1
						if (n+1 <= len(originalID)-1):
							originalID = originalID[n+1:len(originalID)]
							break
			if count >= 15:	
				print ("Pass")	
		else:
			print ("Maybe you write wrong ID number. Please check!")
	elif final_result == None:
		print ("The image is not clear. Please upload again!")
	else:
		print ("Maybe you write wrong ID number. Please check!")
else:
	print ("You write wrong ID number. Please check!")
	
