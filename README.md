# Chinese ID number detector

This project aims to detector ID number on chinese ID photo and compare it with customers' written number corresponding to the photo. The purpose is to remind customers when they wrote a wrong ID number which may cost unnecessary loss, like international espress delivery.

## Getting Started

The project run with OpenCV 3 and PYthon 3.


### Installation

To use this package, you should install OpenCV 3, NumPy, Dlib, Pytesseract, PIL(Python Imaging Library), Scikit-image and Imutils. You can simply install these package by:
```
$ sudo apt-get install build-essential cmake pkg-config
$ sudo apt-get install libx11-dev libatlas-base-dev
$ sudo apt-get install libgtk-3-dev libboost-python-dev
$ python3 -m pip install numpy dlib pytesseract scikit-image imutils
```
For opencv3 in python, you can simply type:  
```
$ python3 -m pip install opencv-python
```
Get into python3 by typing:
```
$ python3
```
Then import opencv by typing:
```
$ import cv2
```

## Image Alignment
We considered that the customers may upload images in different directions. However, the `pytesseract` library works better with horizontal texts. So we implemented image alignment function. The function detects human faces in order to rotate the image to a horizontal direction. 
## Running the tests

The usage of run.py with test images and ID number:
```
$ python3 run.py --image images/image.jpg 123456789123456789
```
`--image` is a fixed string for the code to read images.  
`mages/image.jpg` can be changed to any other paths to your testing photos and it supports all kind of photos.  
`123456789123456789` is the ID number you want to check from your responding ID photo.  
The tests test both your ID card images and judge if the customers wrote the correct number corresponding to the upload images.
### Results
We provided several results to remind customers:
` $ Pass` means the written number is correct.
`$ Maybe you write wrong ID number. Please check!` means our result is not the same as your written number. The customer should check their upload images and the written number.
`$ The image is not clear. Please upload again!` means our detector cannot classify the ID number on your upload images. The customer should check their upload images.
`$ You write wrong ID number. Please check!` means the written ID number must be wrong. The customers should re-write the number correctly.  
The final results reached the accuracy of 81.35%.



## Authors

* **Yining(Winnie) Wu**


## Acknowledgments

* **Adrian Rosebrock** - *Imutils Library* - [jrosebr1](https://github.com/jrosebr1)
* **Nicol** - *Image Processing* - [taozhijiang](https://github.com/taozhijiang)
