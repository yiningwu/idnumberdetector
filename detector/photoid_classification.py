import numpy as np
import re
import pytesseract
from PIL import Image
import cv2
from skimage.morphology import disk
from skimage.filters import rank



def img_sobel_binary(im, blur_sz):
    	# use Gaussian blur and filter excess right angle interference
    	img_blur = cv2.GaussianBlur(im,blur_sz,0)
    	if len(img_blur.shape) == 3:
        	blur_gray = cv2.cvtColor(img_blur,cv2.COLOR_BGR2GRAY) 
    	else:
        	blur_gray = img_blur   
    	# extract Sobel right angle feature, Sobel is more resistant to noise
    	sobelx = cv2.Sobel(blur_gray,cv2.CV_16S,1,0,ksize=3)
    	abs_sobelx = np.absolute(sobelx)     
    	sobel_8u = np.uint8(abs_sobelx)
    	# OTSU is used to extract binary image   
    	ret, thd = cv2.threshold(sobel_8u, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU) 
    	# Scales, calculates absolute values, and converts the result to 8-bit.   
    	thd_abs = cv2.convertScaleAbs(thd)
    	# Calculates the weighted sum of two arrays.
    	bgimg = cv2.addWeighted(thd_abs, 1, 0, 0, 0)    
    	return bgimg

def img_contour_extra(im):
    	# get desired kernal
    	kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(13,9))
    	# dilation followed by Erosion, closing small holes inside the object
    	bgmask = cv2.morphologyEx(im, cv2.MORPH_CLOSE, kernel)
    	# find contours of a binary image
    	im2, contours, hierarchy = cv2.findContours(bgmask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    	return contours

def img_contour_select(ctrs, im):
    	# remove clearly unsatisfied areas
    	cand_rect = []
    	for item in ctrs:
 		# get approximated shape of item
        	epsilon = 0.02*cv2.arcLength(item, True)
        	approx = cv2.approxPolyDP(item, epsilon, True)  
        	if len(approx) <= 8:
            		rect = cv2.minAreaRect(item)
            		if rect[2] < -10 and rect[2] > -80:
                		continue
            		if rect[1][0] < 10 or rect[1][1] < 10:
                		continue
			# get estimated areas
            		box = cv2.boxPoints(rect)
            		box_d = np.int0(box)
            		cand_rect.append(box)
    	return cand_rect

def img_tesseract_detect(c_rect, im):
	#get selected box from image
	Xs = [k[0] for k in c_rect]
	Ys = [k[1] for k in c_rect]
	x1 = min(Xs)
	x2 = max(Xs)
	y1 = min(Ys)
	y2 = max(Ys)
	hight = y2 - y1
	width = x2 - x1
	#cut the image
	warp = im[int(abs(y1)):int(y1+hight), int(abs(x1)):int(x1+width)]
	hh, ww = warp.shape[:2]
	ww = 50*ww//hh
	if ww == 0:
		ww = 1
	#resize the rectangle with fixed height
	warp = cv2.resize(warp, (ww,50))
	# get trimed recognition image
	warp = np.array(warp, dtype=np.uint8)
	radius = 25
	selem = disk(radius)
	# use adaptive OTSU threshold processiong
	local_otsu = rank.otsu(warp, selem)
	l_otsu = np.uint8(warp >= local_otsu)
	l_otsu *= 255
	# get desired kernal
	kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(2,2))
	# dilation followed by Erosion, closing small holes inside the object
	l_otsu = cv2.morphologyEx(l_otsu, cv2.MORPH_CLOSE, kernel)    
	try:
		# transfer image to string using pytesseract
		image_text = pytesseract.image_to_string(Image.fromarray(l_otsu))
		# store string only with number and alphabet
		image_text = re.sub('[^A-Z0-9]','', image_text)
		return image_text
	except:
		return "error........................................."
		print ("------------------")

