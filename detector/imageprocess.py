# import the necessary packages
from .helpers import FACIAL_LANDMARKS_IDXS
from .helpers import shape_to_np
from .helpers import rect_to_bb
from math import *
from .photoid_classification import *
import numpy as np
import cv2
import imutils
import difflib
import re
import dlib


class FaceAligner:
	def __init__(self, predictor, desiredLeftEye=(0.35, 0.35),
		desiredFaceWidth=256, desiredFaceHeight=None):
		# store the facial landmark predictor, desired output left
		# eye position, and desired output face width + height
		self.predictor = predictor
		self.desiredLeftEye = desiredLeftEye
		self.desiredFaceWidth = desiredFaceWidth
		self.desiredFaceHeight = desiredFaceHeight

		# if the desired face height is None, set it to be the
		# desired face width (normal behavior)
		if self.desiredFaceHeight is None:
			self.desiredFaceHeight = self.desiredFaceWidth

	def align(self, image, gray, rect):
		# convert the landmark (x, y)-coordinates to a NumPy array
		shape = self.predictor(gray, rect)
		shape = shape_to_np(shape)

		# extract the left and right eye (x, y)-coordinates
		(lStart, lEnd) = FACIAL_LANDMARKS_IDXS["left_eye"]
		(rStart, rEnd) = FACIAL_LANDMARKS_IDXS["right_eye"]
		leftEyePts = shape[lStart:lEnd]
		rightEyePts = shape[rStart:rEnd]

		# compute the center of mass for each eye
		leftEyeCenter = leftEyePts.mean(axis=0).astype("int")
		rightEyeCenter = rightEyePts.mean(axis=0).astype("int")

		# compute the angle between the eye centroids
		dY = rightEyeCenter[1] - leftEyeCenter[1]
		dX = rightEyeCenter[0] - leftEyeCenter[0]
		angle = np.degrees(np.arctan2(dY, dX)) - 180

		# compute the desired right eye x-coordinate based on the
		# desired x-coordinate of the left eye
		desiredRightEyeX = 1.0 - self.desiredLeftEye[0]

		# determine the scale of the new resulting image by taking
		# the ratio of the distance between eyes in the *current*
		# image to the ratio of distance between eyes in the
		# *desired* image
		dist = np.sqrt((dX ** 2) + (dY ** 2))
		desiredDist = (desiredRightEyeX - self.desiredLeftEye[0])
		desiredDist *= self.desiredFaceWidth
		scale = desiredDist / dist

		# grab the rotation matrix for rotating winnie modified 14/8/17
		h, w = image.shape[:2]
		hnew = int(w*fabs(sin(radians(angle)))+h*fabs(cos(radians(angle))))
		wnew = int(h*fabs(sin(radians(angle)))+w*fabs(cos(radians(angle))))
		M = cv2.getRotationMatrix2D((w/2,h/2), angle, 1)
		M[0, 2] += (wnew-w)/2
		M[1, 2] += (hnew-h)/2

		# apply the affine transformation
		output = cv2.warpAffine(image, M, (wnew,hnew),flags=cv2.INTER_LANCZOS4)

		# return the aligned face
		return output


#get the text of highest similarity with original ID number 
def image_processing(img, IDnumber):
	highest_ratio = 0
	detected_no = None
	# get dlib frontal face detector
	# get dlib shape predictor and call facealigner
	detector = dlib.get_frontal_face_detector()
	predictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")
	fa = FaceAligner(predictor, desiredFaceWidth=256)
	for degree in (0,90,180,270):
		x = 0
		similarity_ratio = 0
		final_result = "a"
		img = image_rotated(degree, img)
		img = imutils.resize(img, width = 600)
		#call functions in photoid_classification
		img_gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
		sb_img = img_sobel_binary(img, (7,7))
		contours = img_contour_extra(sb_img)
		cand_rect = img_contour_select(contours, img)
		if cand_rect != None:
			for item in cand_rect:
				id_no = img_tesseract_detect(item, img_gray)
				id_no_len = len(id_no)
				#get the longest text
				if id_no_len > x:
			    		x = id_no_len
			    		final_result = id_no
			#calculate the similarity
     			#number of same characters (n)
			#the lenth of string a(lena) and the lenth of string b(lenb)
			#ratio = 2*n/(lena+lenab)
			s = difflib.SequenceMatcher(None, final_result, IDnumber)
			similarity_ratio = s.ratio()
			if (similarity_ratio == 1):
				detected_no = final_result
				break
			#compare the ratio
			elif (similarity_ratio > highest_ratio):
				detected_no = final_result
				highest_ratio = similarity_ratio
			else:
				rects = detector(img_gray, 2)
				if rects != None:
					for rect in rects:
						(x, y, w, h) = rect_to_bb(rect)
						img = fa.align(img, img_gray, rect)
					img = imutils.resize(img, width = 600)
					img_gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
					sb_img = img_sobel_binary(img, (7,7))
					contours = img_contour_extra(sb_img)
					cand_rect = img_contour_select(contours, img)
					x = 0
					final_result = "a"
					if cand_rect != None:
						for item in cand_rect:
							id_no = img_tesseract_detect(item, img_gray)
							id_no_len = len(id_no)

							if id_no_len > x:
					    			x = id_no_len
					    			final_result = id_no
						s = difflib.SequenceMatcher(None, final_result, IDnumber)
						similarity_ratio = s.ratio()
						if (similarity_ratio == 1):
							detected_no = final_result
							break
						elif (similarity_ratio > highest_ratio):
							detected_no = final_result
							highest_ratio = similarity_ratio
		else:
			rects = detector(img_gray, 2)
			if rects != None:
				for rect in rects:
					(x, y, w, h) = rect_to_bb(rect)
					img = fa.align(img, img_gray, rect)
				img = imutils.resize(img, width = 600)
				img_gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
				sb_img = img_sobel_binary(img, (7,7))
				contours = img_contour_extra(sb_img)
				cand_rect = img_contour_select(contours, img)
				x = 0
				final_result = "a"
				if cand_rect != None:
					for item in cand_rect:
						id_no = img_tesseract_detect(item, img_gray)
						id_no_len = len(id_no)

						if id_no_len > x:
				    			x = id_no_len
				    			final_result = id_no
					s = difflib.SequenceMatcher(None, final_result, IDnumber)
					similarity_ratio = s.ratio()
					if (similarity_ratio == 1):
						detected_no = final_result
						break
					elif (similarity_ratio > highest_ratio):
						detected_no = final_result
						highest_ratio = similarity_ratio		
	return (detected_no, highest_ratio)

def image_rotated(degree, img):
	#calculate new with and height after rotation
	height, width = img.shape[:2]
	heightNew = int(width*fabs(sin(radians(degree)))+height*fabs(cos(radians(degree))))
	widthNew = int(height*fabs(sin(radians(degree)))+width*fabs(cos(radians(degree))))
	#calculate rotation matrix with given degree
	matRotation = cv2.getRotationMatrix2D((width/2, height/2), degree, 1)
	matRotation[0,2] += (widthNew - width)/2
	matRotation[1,2] += (heightNew - height)/2
	#rotate the image and return
	imgRotation = cv2.warpAffine(img, matRotation, (widthNew, heightNew), borderValue=(255,255,255))
	return imgRotation

